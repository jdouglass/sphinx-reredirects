Release notes
#############

0.0.1 (2021-04-26)
******************

- bugfix wildcard/non-wildcard implementation and code clean-up
- migrate README to Sphinx docs
- rewrite Usage article in docs

0.0.0 (2020-09-09)
******************

Initial release.